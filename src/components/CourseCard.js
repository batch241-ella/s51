import { useState } from "react";

import { Card, Button } from "react-bootstrap";

export default function CourseCard({course}) {

	const {name, description, price} = course;
	// Use the state hook for this component to store its state
	// States are used to keep track of information related to individual components
	/*
		SYNTAX
		const [getter, setter] = useState(initialGetterValue)
	*/
	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(30);

	function enroll() {
		if (seats == 0) {
			alert("No more seats.");
		} else {
			setCount(count + 1);
			setSeats(seats - 1);
		}
	}



	return (
	<Card>
	    <Card.Body>
	        <Card.Title>{name}</Card.Title>
	        <Card.Subtitle>Description:</Card.Subtitle>
	        <Card.Text>{description}</Card.Text>
	        <Card.Subtitle>Price:</Card.Subtitle>
	        <Card.Text>PhP {price}</Card.Text>
	        <Card.Text>Enrollees:  {count}</Card.Text>
	        {/*<Button variant="primary">Enroll</Button>*/}
	        <Button className="bg-primary" onClick={enroll}>Enroll</Button>
	    </Card.Body>
	</Card>
	)
}