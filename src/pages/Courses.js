import coursesData from '../data/coursesData';
import CourseCard from "../components/CourseCard";

export default function Courses() {
	console.log(coursesData);
	console.log(coursesData[0]);

	const courses = coursesData.map(course => {
		return (
			<CourseCard key={course.id} course={course} />
		)
	});

	return (
		<>
		{courses}
		{/* Props Drilling - we can pass information from one component to another using props */}
		{/* {} - used in props to signify that we are providing information */}
		{/*<CourseCard courseProp = {coursesData[0]} />*/}
		</>
	)
}